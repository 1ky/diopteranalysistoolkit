import os
import re
from bs4 import BeautifulSoup

# Get List of valid files
def file_list(root='../source_files/'):
    if root[-1] != '/':
        root = root + '/'
        
    l = []
    for r, dirs, files in os.walk(root):
        for f in files:
            if (f.endswith('.html') or f.endswith('.mht') or f.endswith('.xml')
                or f.endswith('.htm')):
                l.append(os.path.join(r, f))
            
    return l

# Get Batch ID from file
def id_scan(f_path):
    with open(f_path, 'r', errors='ignore') as f_obj:
        f_content = f_obj.read()
        
    f_soup = BeautifulSoup(f_content, 'lxml')
    #print(f_path)
    return f_soup.find('link', href=True)

# Get Batch ID from file
def id_alt_scan(f_path):
    with open(f_path, 'r', errors='ignore') as f_obj:
        f_content = f_obj.read()
        
    f_soup = BeautifulSoup(f_content, 'lxml')
    s = []
    st = ''
    #print(f_path)
    try:
        s = f_soup.find_all('p')
        if id_cleaner(str(s[3])) != None:
            st = id_cleaner(str(s[3]))
        elif id_cleaner(str(s[4])) != None:
            st = id_cleaner(str(s[4]))
    except IndexError:
        print('Out of range ',end='')
    
    return st

# Finds Batch ID within text string
def id_cleaner(s):
    extn_regex = re.compile(r'(S|SB|F)?\d{4,6}')
    st = extn_regex.search(s)
    try:
        return st.group()
    except AttributeError:
        #print('None object')
        return None
    
# Removes Space Characters From Strings
def rm_sp(s):
    data_regex = re.compile(r'(\s+|=)')
    if data_regex.findall(s):
        s = data_regex.sub(r'',s)
    return s