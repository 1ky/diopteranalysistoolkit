from DAT_DataScraperFunctions import (file_list, id_scan, id_alt_scan, id_cleaner)
from DAT_DatabaseFunctions import (get_db_cursor, is_valid_batch_code, ins_db_data)

if __name__ == '__main__':
    l = file_list('../DAT_Data/Rotlex Data Files August - wk1September/')
    db_cursor = get_db_cursor()
        
    for f_name in l:
        s = id_scan(f_name)
        s_alt = id_alt_scan(f_name)
        batchID = ''
        
        if s != None:
            #print('Cleaner 1')
            batchID = id_cleaner(str(s))
            #print(batchID)
            #print()
        elif s_alt != None:
            #print('Cleaner 2')
            #print(s_alt)
            batchID = id_cleaner(str(s_alt))
            #print(batchID)
            #print()
        
        batchIDwithF = ''
        if batchID != None or batchID != '':
            batchIDwithF = 'F' + str(batchID)
        
        if is_valid_batch_code(db_cursor, batchID):
            print(f'extracting data for batch {batchID}')
            ins_db_data(f_name, db_cursor, batchID)
        elif is_valid_batch_code(db_cursor, batchIDwithF):
            print(f'extracting data for batch {batchIDwithF}')
            ins_db_data(f_name, db_cursor, batchIDwithF)
            
        db_cursor.commit()

    print('Done')