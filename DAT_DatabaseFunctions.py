import pyodbc
import datetime
from bs4 import BeautifulSoup
from DAT_DataScraperFunctions import rm_sp

# Creates a Database Object Cursor
def get_db_cursor(database='Database=LensData;'):
    driver = 'Driver={SQL Server Native Client 11.0};'
    server = 'Server=localhost;'
    trust = 'Trusted_Connection=yes;'

    try:
        db_con = pyodbc.connect(driver+server+database+trust)
    except:
        print('connection to database error')
        
    return db_con.cursor()

# Searches Database for Batch ID
def is_valid_batch_code(db_cursor, batchID):
    db_cursor.execute("""
        SELECT PowerShift.BatchCode 
        FROM PowerShift
        WHERE BatchCode = ?
            AND (PowerShift.ModelName = 'SOFTEC I'
                OR PowerShift.ModelName = 'SOFTEC HD'
                OR PowerShift.ModelName = 'SOFTEC HDO')
        """, batchID)
    
    if db_cursor.fetchall() == []:
        return False
    
    return True

# Parses HTML Files and Inserts Into Database
def ins_db_data(f_name, db_cursor, batchID):   
    with open(f_name, 'r', errors='ignore') as f_obj:
        f_content = f_obj.read()
        
    f_soup = BeautifulSoup(f_content, 'lxml')
    table = f_soup.find_all('table')[-1]
    
    r_counter = 0
    for row in table.find_all('tr'):
        columns = row.find_all('td')
        data = []
        c_counter = 0
        if r_counter > 0:
            if len(columns) == 8:
                for c in columns:
                    if c_counter == 1:
                        data.append(batchID)
                        try:
                            data.append(int(rm_sp(c.get_text())))
                        except ValueError:
                            print(f'invalid label value in batch {f_name} on line {r_counter}')
                    if c_counter == 3:
                        try:
                            date_obj = datetime.datetime.strptime(rm_sp(c.get_text()), '%d-%m-%Y')
                            data.append(date_obj)
                        except ValueError:
                            print(f'invalid time value in batch {f_name} on line {r_counter}')
                    if c_counter == 4:
                        try:
                            date_obj = datetime.datetime.strptime(rm_sp(c.get_text()), '%H:%M')
                            data.append(date_obj)
                        except ValueError:
                            print(f'invalid time value in batch {f_name} on line {r_counter}')
                    if c_counter == 5:
                        try:
                            data.append(float(rm_sp(c.get_text())))
                        except ValueError:
                            print(f'invalid power value in batch {f_name} on line {r_counter}')
                    if c_counter == 6:
                        try:
                            data.append(float(rm_sp(c.get_text())))
                        except ValueError:
                            print(f'invalid quality value in batch {f_name} on line {r_counter}')
                    if c_counter == 7:
                        try:
                            data.append(float(rm_sp(c.get_text())))
                        except ValueError:
                            print(f'invalid power value in batch {f_name} on line {r_counter}')
                    c_counter += 1
            elif len(columns) == 9:
                for c in columns:
                    if c_counter == 1:
                        data.append(batchID)
                        try:
                            data.append(int(rm_sp(c.get_text())))
                        except ValueError:
                            print(f'invalid label value in batch {f_name} on line {r_counter}')
                    if c_counter == 2:                       
                        try:
                            date_obj = datetime.datetime.strptime(rm_sp(c.get_text()), '%d-%m-%Y')
                            data.append(date_obj)
                        except ValueError:
                            print(f'invalid time value in batch {f_name} on line {r_counter}')
                    if c_counter == 3:
                        try:
                            date_obj = datetime.datetime.strptime(rm_sp(c.get_text()), '%H:%M')
                            data.append(date_obj)
                        except ValueError:
                            print(f'invalid time value in batch {f_name} on line {r_counter}')
                    if c_counter == 4:
                        try:
                            data.append(float(rm_sp(c.get_text())))
                        except ValueError:
                            print(f'invalid power value in batch {f_name} on line {r_counter}')
                    if c_counter == 6:
                        try:
                            data.append(float(rm_sp(c.get_text())))
                        except:
                            print(f'invalid quality value in batch {f_name} on line {r_counter}')
                    if c_counter == 8:
                        try:
                            data.append(float(rm_sp(c.get_text())))
                        except:
                            print(f'invalid power group in batch {f_name} on line {r_counter}')
                    c_counter += 1
            elif len(columns) == 10:
                for c in columns:
                    if c_counter == 1:
                        data.append(batchID)
                        try:
                            data.append(int(rm_sp(c.get_text())))
                        except ValueError:
                            print(f'invalid label value in batch {f_name} on line {r_counter}')
                    if c_counter == 2:
                        try:
                            date_obj = datetime.datetime.strptime(rm_sp(c.get_text()), '%d-%m-%Y')
                            data.append(date_obj)
                        except ValueError:
                            print(f'invalid time value in batch {f_name} on line {r_counter}')
                    if c_counter == 3:
                        try:
                            date_obj = datetime.datetime.strptime(rm_sp(c.get_text()), '%H:%M')
                            data.append(date_obj)
                        except ValueError:
                            print(f'invalid time value in batch {f_name} on line {r_counter}')
                    if c_counter == 4:
                        try:
                            data.append(float(rm_sp(c.get_text())))
                        except ValueError:
                            print(f'invalid power value in batch {f_name} on line {r_counter}')
                    if c_counter == 7:
                        try:
                            data.append(float(rm_sp(c.get_text())))
                        except:
                            print(f'invalid quality value in batch {f_name} on line {r_counter}')
                    if c_counter == 9:
                        try:
                            data.append(float(rm_sp(c.get_text())))
                        except:
                            print(f'invalid power group in batch {f_name} on line {r_counter}')
                    c_counter += 1
            else:
                print(f'Unknown file format {f_name}')
                 
            if len(data) == 7:
                try:
                    db_cursor.execute('''
                        INSERT INTO Lenses
                        (BatchCode, LensLabel, LensDate, LensTime, PowerValue, LensQuality, PowerGroup)
                        VALUES (?, ?, ?, ?, ?, ?, ?)''', data)
                except:
                    print(f'data in file {f_name} already in database {data}')
            else:
                print(f'error: less than 7 parameters - {f_name}')
        r_counter += 1