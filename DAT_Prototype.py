# -*- coding: utf-8 -*-
"""
Created on Thu Aug  1 10:16:45 2019

@author: Rex
"""

import sys
import sqlalchemy
import numpy as np
import pandas as pd
import scipy.stats as stats
import matplotlib.pyplot as plt

from PyQt5.QtWidgets import (QDesktopWidget, QMainWindow, QAction, QFileDialog,
                             QHBoxLayout, QVBoxLayout, QFrame, QTableView,
                             QSplitter, QLineEdit, QComboBox, QLabel,
                             QMessageBox, QPushButton, qApp, QApplication,
                             QCalendarWidget, QWidget)
from PyQt5.QtGui import QIcon
from PyQt5.QtCore import Qt, QDate
from DAT_PandasModel import PandasModel

class DA(QMainWindow):
    def __init__(self):
        super().__init__()
        
        self.initUI()
        
    def initUI(self):
        # Menubar
        menubar = self.menuBar()
        fileMenu = menubar.addMenu('File')
        dataMenu = menubar.addMenu('Data')
        
        # File Menu
        openAct = QAction(QIcon(), '&Open...', self)
        saveAct = QAction(QIcon(), '&Save As...', self)
        exitAct = QAction(QIcon(), '&Exit', self)
        
        openAct.triggered.connect(self.importDataframe)
        saveAct.triggered.connect(self.exportDataframe)
        exitAct.triggered.connect(qApp.quit)
        
        openAct.setStatusTip('Open Datafile')
        saveAct.setStatusTip('Save Data As...')
        exitAct.setStatusTip('Exit Application')
        
        fileMenu.addAction(openAct)
        fileMenu.addAction(saveAct)
        fileMenu.addAction(exitAct)
        
        # Data Menu
        runAct = QAction(QIcon(), '&Activate', self)
        clearAct = QAction(QIcon(), '&Clear Table', self)
        
        runAct.triggered.connect(self.loadDataframe)
        clearAct.triggered.connect(self.clearDataframe)
        
        runAct.setStatusTip('Run Using Current Parameters')
        clearAct.setStatusTip('Clear Table')
        
        dataMenu.addAction(runAct)
        dataMenu.addAction(clearAct)
        
        # Central Window Layout
        self.optionsWindow = QFrame()
        self.mainWindow = QFrame()
        self.optionsWindow.setFrameShape(QFrame.StyledPanel | QFrame.Sunken)
        self.mainWindow.setFrameShape(QFrame.StyledPanel | QFrame.Sunken)
        
        splitter = QSplitter(Qt.Horizontal)
        splitter.addWidget(self.optionsWindow)
        splitter.addWidget(self.mainWindow)
        self.setCentralWidget(splitter)
        
        # Dataframe and Current Date
        self.df = pd.DataFrame()
        self.calDateISO = QDate.currentDate().toString(Qt.ISODate)
        
        # Query Parameters
        self.batchQuery = ''
        self.modelQuery = ''
        self.launchQuery = ''
        self.diopterQuery = ''
        self.iolaQuery = ''
        self.firstLatheQuery = ''
        self.secondLatheQuery = ''
        self.dateMinQuery = ''
        self.dateMaxQuery = ''
        
        # Option Window Layout
        batchLbl = QLabel(self)
        modelLbl = QLabel(self)
        launchLbl = QLabel(self)
        iolaLbl = QLabel(self)
        firstLatheLbl = QLabel(self)
        secondLatheLbl = QLabel(self)
        dateLbl = QLabel(self)
        dateRangeLbl = QLabel(self)
        
        batchLbl.resize(60,20)
        modelLbl.resize(60,20)
        launchLbl.resize(60,20)
        iolaLbl.resize(60,20)
        firstLatheLbl.resize(60,20)
        secondLatheLbl.resize(60,20)
        dateLbl.resize(60,20)
        dateRangeLbl.resize(2,20)
        
        batchLbl.setText('Lens Batch ID')
        modelLbl.setText('Lens Model')
        launchLbl.setText('Launched As')
        iolaLbl.setText('Rotlex Iola ID')
        firstLatheLbl.setText('First Cut Lathe')
        secondLatheLbl.setText('Second Cut Lathe')
        dateLbl.setText('Lens Date Range')
        dateRangeLbl.setText('to')
        
        # Buttons, Boxes and Line Edits
        self.batchEdit = QLineEdit()
        
        self.modelBox = QComboBox(self.optionsWindow)
        self.launchBox = QComboBox(self.optionsWindow)
        self.iolaBox = QComboBox(self.optionsWindow)
        self.firstLatheBox = QComboBox(self.optionsWindow)
        self.secondLatheBox = QComboBox(self.optionsWindow)
        
        self.modelBox.addItem('---')
        self.modelBox.addItem('Softec I')
        self.modelBox.addItem('Softec HD')
        self.modelBox.addItem('Softec HDO')
        
        self.launchBox.addItem('---')
        self.launchBox.addItem('-12.0')
        self.launchBox.addItem('-11.0')
        self.launchBox.addItem('-5.0')
        self.launchBox.addItem('-4.0')
        self.launchBox.addItem('-3.0')
        self.launchBox.addItem('-2.0')
        self.launchBox.addItem('-1.0')
        self.launchBox.addItem('0')
        self.launchBox.addItem('1.0')
        self.launchBox.addItem('2.0')
        self.launchBox.addItem('3.0')
        self.launchBox.addItem('4.0')
        self.launchBox.addItem('5.0')
        self.launchBox.addItem('6.0')
        self.launchBox.addItem('7.0')
        self.launchBox.addItem('8.0')
        self.launchBox.addItem('9.0')
        self.launchBox.addItem('10.0')
        self.launchBox.addItem('10.5')
        self.launchBox.addItem('11.0')
        self.launchBox.addItem('11.5')
        self.launchBox.addItem('12.0')
        self.launchBox.addItem('12.5')
        self.launchBox.addItem('13.0')
        self.launchBox.addItem('13.5')
        self.launchBox.addItem('14.0')
        self.launchBox.addItem('14.5')
        self.launchBox.addItem('15.0')
        self.launchBox.addItem('15.25')
        self.launchBox.addItem('15.5')
        self.launchBox.addItem('15.75')
        self.launchBox.addItem('16.0')
        self.launchBox.addItem('16.25')
        self.launchBox.addItem('16.5')
        self.launchBox.addItem('16.75')
        self.launchBox.addItem('17.0')
        self.launchBox.addItem('17.25')
        self.launchBox.addItem('17.5')
        self.launchBox.addItem('17.75')
        self.launchBox.addItem('18.0')
        self.launchBox.addItem('18.25')
        self.launchBox.addItem('18.5')
        self.launchBox.addItem('18.75')
        self.launchBox.addItem('19.0')
        self.launchBox.addItem('19.25')
        self.launchBox.addItem('19.5')
        self.launchBox.addItem('19.75')
        self.launchBox.addItem('20.0')
        self.launchBox.addItem('20.25')
        self.launchBox.addItem('20.5')
        self.launchBox.addItem('20.75')
        self.launchBox.addItem('21.0')
        self.launchBox.addItem('21.25')
        self.launchBox.addItem('21.5')
        self.launchBox.addItem('21.75')
        self.launchBox.addItem('22.0')
        self.launchBox.addItem('22.25')
        self.launchBox.addItem('22.5')
        self.launchBox.addItem('22.75')
        self.launchBox.addItem('23.0')
        self.launchBox.addItem('23.25')
        self.launchBox.addItem('23.5')
        self.launchBox.addItem('23.75')
        self.launchBox.addItem('24.0')
        self.launchBox.addItem('24.25')
        self.launchBox.addItem('24.5')
        self.launchBox.addItem('24.75')
        self.launchBox.addItem('25.0')
        self.launchBox.addItem('25.5')
        self.launchBox.addItem('26.0')
        self.launchBox.addItem('26.5')
        self.launchBox.addItem('27.0')
        self.launchBox.addItem('27.5')
        self.launchBox.addItem('28.0')
        self.launchBox.addItem('28.5')
        self.launchBox.addItem('29.0')
        self.launchBox.addItem('29.5')
        self.launchBox.addItem('30.0')
        self.launchBox.addItem('31.0')
        self.launchBox.addItem('32.0')
        self.launchBox.addItem('33.0')
        self.launchBox.addItem('34.0')
        self.launchBox.addItem('35.0')
        self.launchBox.addItem('36.0')
        
        self.iolaBox.addItem('---')
        self.iolaBox.addItem('03-0267')
        self.iolaBox.addItem('03-0472')
        self.iolaBox.addItem('13-0267')
        self.iolaBox.addItem('03-0462')
        self.iolaBox.addItem('03-0269')
        self.iolaBox.addItem('O3-0467')
        self.iolaBox.addItem('03-0564')
        self.iolaBox.addItem('03-0168')
        self.iolaBox.addItem('93-0168')
        self.iolaBox.addItem('03-0565')
        self.iolaBox.addItem('03-0457')
        
        self.firstLatheBox.addItem('---')
        self.firstLatheBox.addItem('01-0309')
        self.firstLatheBox.addItem('03-0007')
        self.firstLatheBox.addItem('01+-0205')
        self.firstLatheBox.addItem('01-0005')
        self.firstLatheBox.addItem('01-005')
        self.firstLatheBox.addItem('01-0004/01')
        self.firstLatheBox.addItem('01-0100')
        self.firstLatheBox.addItem('0-0173')
        self.firstLatheBox.addItem('01-174')
        self.firstLatheBox.addItem('01-0173/01')
        self.firstLatheBox.addItem('01-209')
        self.firstLatheBox.addItem('01-0008')
        self.firstLatheBox.addItem('01-01-205')
        self.firstLatheBox.addItem('01-0101')
        self.firstLatheBox.addItem('01-0385')
        self.firstLatheBox.addItem('01-0166')
        self.firstLatheBox.addItem('01-0173')
        self.firstLatheBox.addItem('01-0099')
        self.firstLatheBox.addItem('01-205')
        self.firstLatheBox.addItem('01-0007')
        self.firstLatheBox.addItem('01-0208')
        self.firstLatheBox.addItem('01-0028')
        self.firstLatheBox.addItem('01-0174')
        
        self.secondLatheBox.addItem('---')
        self.secondLatheBox.addItem('01-0241')
        self.secondLatheBox.addItem('01-0205')
        self.secondLatheBox.addItem('01-0028/01')
        self.secondLatheBox.addItem('01-0005')
        self.secondLatheBox.addItem('01-208')
        self.secondLatheBox.addItem('01-0100')
        self.secondLatheBox.addItem('01-0008')
        self.secondLatheBox.addItem('01-386')
        self.secondLatheBox.addItem('.367')
        self.secondLatheBox.addItem('01-0101')
        self.secondLatheBox.addItem('01-0385')
        self.secondLatheBox.addItem('01-0009')
        self.secondLatheBox.addItem('01-0166')
        self.secondLatheBox.addItem('01-0173')
        self.secondLatheBox.addItem('.374')
        self.secondLatheBox.addItem('01-010')
        self.secondLatheBox.addItem('01-0306')
        self.secondLatheBox.addItem('.01-0100')
        self.secondLatheBox.addItem('01-0100/01')
        self.secondLatheBox.addItem('01-0007')
        self.secondLatheBox.addItem('01-0208')
        self.secondLatheBox.addItem('01-0028')
        self.secondLatheBox.addItem('01-0174')
        
        self.dateMinEdit = QLineEdit()
        self.dateMaxEdit = QLineEdit()       
        self.dateRangeCal = QPushButton('Set Date Range', self.optionsWindow)
            
        vOp = QVBoxLayout(self.optionsWindow)
        hOp3 = QHBoxLayout(self)
        
        vOp.addWidget(batchLbl)
        vOp.addWidget(self.batchEdit)
        
        vOp.addWidget(modelLbl)
        vOp.addWidget(self.modelBox)
        
        vOp.addWidget(launchLbl)
        vOp.addWidget(self.launchBox)
        
        vOp.addWidget(iolaLbl)
        vOp.addWidget(self.iolaBox)
        
        vOp.addWidget(firstLatheLbl)
        vOp.addWidget(self.firstLatheBox)
        
        vOp.addWidget(secondLatheLbl)
        vOp.addWidget(self.secondLatheBox)
        
        vOp.addWidget(dateLbl)
        vOp.addWidget(self.dateRangeCal)
        hOp3.addWidget(self.dateMinEdit)
        hOp3.addWidget(dateRangeLbl)
        hOp3.addWidget(self.dateMaxEdit)
        vOp.addLayout(hOp3)
        
        # Option Functions
        self.batchEdit.textChanged[str].connect(self.onBatch)
        self.modelBox.activated[str].connect(self.onModel)
        self.launchBox.activated[str].connect(self.onLaunch)
        self.iolaBox.activated[str].connect(self.onIola)
        self.firstLatheBox.activated[str].connect(self.onFirstLathe)
        self.secondLatheBox.activated[str].connect(self.onSecondLathe)
        self.dateMinEdit.textChanged[str].connect(self.onDateMin)
        self.dateMaxEdit.textChanged[str].connect(self.onDateMax)
        self.dateRangeCal.clicked.connect(self.onDateCal)
        
        # Dataframe Window Layout
        vbox = QVBoxLayout(self.mainWindow)
        hbox = QHBoxLayout()
        
        self.loadBtn = QPushButton('Activate', self.mainWindow)
        self.importBtn = QPushButton('Import', self.mainWindow)
        self.exportBtn = QPushButton('Export', self.mainWindow)
        self.graphBtn = QPushButton('Graph', self.mainWindow)
        self.clearBtn = QPushButton('Clear', self.mainWindow)
        self.pandasTv = QTableView(self.mainWindow)
        
        self.loadBtn.clicked.connect(self.loadDataframe)
        self.importBtn.clicked.connect(self.importDataframe)
        self.exportBtn.clicked.connect(self.exportDataframe)
        self.graphBtn.clicked.connect(self.graphDataframe)
        self.clearBtn.clicked.connect(self.clearDataframe)
        self.pandasTv.setSortingEnabled(True)
        
        self.loadBtn.setStatusTip('Run Using Current Parameters')
        self.importBtn.setStatusTip('Open Datafile')
        self.exportBtn.setStatusTip('Save Data As...')
        self.graphBtn.setStatusTip('Show Normal Distribution of Data')
        self.clearBtn.setStatusTip('Clear Table')
        
        hbox.addWidget(self.loadBtn)
        hbox.addWidget(self.importBtn)
        hbox.addWidget(self.exportBtn)
        hbox.addWidget(self.graphBtn)
        hbox.addWidget(self.clearBtn)
        vbox.addLayout(hbox)
        vbox.addWidget(self.pandasTv)
        
        self.statusBar().showMessage('Ready')
        
        self.resize(1100,600)
        self.center()
        self.setWindowTitle('Diopter Analysis Toolkit')
        self.show()
        
    def center(self):
        qr = self.frameGeometry()
        cp = QDesktopWidget().availableGeometry().center()
        qr.moveCenter(cp)
        self.move(qr.topLeft())
        
    def onBatch(self, text):
        if text == '':
            self.batchQuery = ''
        else:
            self.batchQuery = "AND Lenses.BatchCode LIKE '" + text.upper() + "%'"
        
    def onModel(self, text):
        if text == '---':
            self.modelQuery = ''
        else:
            self.modelQuery = "AND PowerShift.ModelName = '" + text + "'"
            
    def onLaunch(self, text):
        if text == '---':
            self.launchQuery = ''
        else:
            self.launchQuery = "AND PowerShift.Diopter = " + text
            
    def onIola(self, text):
        if text == '':
            self.iolaQuery = ''
        else:
            self.iolaQuery = "AND PowerShift.IolaID = '" + text + "'"
            
    def onFirstLathe(self, text):
        if text == '---':
            self.firstLatheQuery = ''
        else:
            self.firstLatheQuery = "AND PowerShift.FirstCutLathe = '" + text + "'"
            
    def onSecondLathe(self, text):
        if text == '':
            self.secondLatheQuery = ''
        else:
            self.secondLatheQuery = "AND PowerShift.SecondCutLathe = '" + text + "'"
        
            
    def onDateMin(self, text):
        if text == '':
            self.dateMinQuery = ''
        else:
            self.dateMinQuery = "AND Lenses.LensDate >= '" + text + "'"
        
    def onDateMax(self, text):
        if text == '':
            self.dateMaxQuery = ''
        else:
            self.dateMaxQuery = "AND Lenses.LensDate <= '" + text + "'"
            
    def onDateCal(self):
        self.dateWindow = QWidget()
        
        self.cal = QCalendarWidget(self)
        self.cal.setGridVisible(True)
        self.calDateFromBtn = QPushButton('Set Date From', self)
        self.calDateToBtn = QPushButton('Set Date To', self)
        self.calLabel = QLabel(self)
        
        self.calDate = self.cal.selectedDate()
        self.calLabel.setText(self.calDate.toString())
        
        self.calVbox = QVBoxLayout()
        self.calHbox = QHBoxLayout()
        
        self.calHbox.addWidget(self.cal)
        self.calHbox.addWidget(self.calDateFromBtn)
        self.calHbox.addWidget(self.calDateToBtn)
        self.calVbox.addLayout(self.calHbox)
        self.calVbox.addWidget(self.calLabel)
        self.dateWindow.setLayout(self.calVbox)
        
        self.cal.clicked[QDate].connect(self.showCalDate)
        self.calDateFromBtn.clicked.connect(self.setDateMin)
        self.calDateToBtn.clicked.connect(self.setDateMax)
        
        self.dateWindow.setGeometry(300, 300, 350, 300)
        self.dateWindow.setWindowTitle('Calendar')

        self.dateWindow.show()
        
    def showCalDate(self, date):
        self.calLabel.setText(date.toString())
        self.calDateISO = date.toString(Qt.ISODate)
        
    def setDateMin(self):
        self.dateMinEdit.setText(self.calDateISO)
        
    def setDateMax(self):
        self.dateMaxEdit.setText(self.calDateISO)
        
    def loadDataframe(self):
        engine = sqlalchemy.create_engine('mssql+pyodbc://localhost/LensData?driver=SQL+Server+Native+Client+11.0')
        query = """
                SELECT Lenses.BatchCode, PowerShift.ModelName, Lenses.LensDate, PowerShift.Diopter AS LaunchedAs, PowerShift.OldDiopter AS ShiftedFrom,
                Lenses.PowerValue, PowerShift.IolaID AS Rotlex, PowerShift.FirstCutLathe, PowerShift.SecondCutLathe, PowerShift.FirstCutBFL, PowerShift.SecondCutBFL,
                Lenses.PowerGroup, Lenses.LensQuality
                FROM Lenses, PowerShift
                WHERE Lenses.BatchCode = PowerShift.BatchCode
                {}
                {}
                {}
                {}
                {}
                {}
                {}
                {}
                """.format(self.batchQuery, self.modelQuery, self.launchQuery,
                self.firstLatheQuery, self.secondLatheQuery, self.dateMinQuery,
                self.iolaQuery, self.dateMaxQuery)

        self.df = pd.read_sql_query(query, engine)
        self.df.set_index('ModelName', inplace=True)
        model = PandasModel(self.df)
        self.pandasTv.setModel(model)
        
    def importDataframe(self):
        xlsx_file = QFileDialog.getOpenFileName(self, 'Select File', 'C:/Users/Rex/Desktop')
        if not xlsx_file[0] == '':
            wb = pd.ExcelFile(xlsx_file[0])
        
            sheet = wb.sheet_names
            self.df = wb.parse(sheet[0])
            #self.df.set_index('ModelName', inplace=True)
            model = PandasModel(self.df)
            self.pandasTv.setModel(model)
        
    def exportDataframe(self):
        xlsx_file = QFileDialog.getSaveFileName(self, 'Save File', 'C:/Users/Rex/Desktop', '.xlsx')
        
        self.df.to_excel(xlsx_file[0] + xlsx_file[1])
        
    def graphDataframe(self):
        plt.figure()
        plt.style.use('ggplot')
        
        df_list = self.df['PowerValue'].tolist()
        df_list.sort()
        
        total = len(df_list)
        mean = np.mean(df_list)
        std = np.std(df_list)
        plus_std = mean + std
        minus_std = mean - std
        fit = stats.norm.pdf(df_list, mean, std)
        
        above_std = 0
        below_std = 0
        within_std = 0
        for lens in df_list:
            if lens >= plus_std:
                above_std += 1
            elif lens <= minus_std:
                below_std += 1
            else:
                within_std += 1
        above_percentage = (above_std/total)*100
        below_percentage = (below_std/total)*100
        within_percentage = (within_std/total)*100
        
        plt.title('Lens Power Distribution')
        plt.xlabel('Diopter')
        
        mean_legend = 'Mean Power: %.2f (%d lenses)' % (mean, total)
        within_legend = 'Within 1 Standard Diviation: %d lenses (%.1f%%)' % (within_std, within_percentage)
        above_legend = 'Above 1 Standard Diviation: %d lenses (%.1f%%)' % (above_std, above_percentage)
        below_legend = 'Below 1 Standard Diviation: %d lenses (%.1f%%)' % (below_std, below_percentage)
        
        plt.plot(df_list, fit, '-')
        plt.vlines(mean, 0, max(fit), linewidth=2, alpha=0.7, label=mean_legend)
        plt.fill_between(df_list, fit, color='#05a05a', alpha=0.5, label=within_legend)
        plt.fill_between(df_list, fit, where=(df_list >= plus_std), alpha=0.5, label=above_legend)
        plt.fill_between(df_list, fit, where=(df_list <= minus_std), alpha=0.5, label=below_legend)
        
        plt.tight_layout()
        plt.legend()
        plt.show()
        
    def clearDataframe(self):
        self.df = pd.DataFrame()
        model = PandasModel(self.df)
        self.pandasTv.setModel(model)
        
    def closeEvent(self, e):
        reply = QMessageBox.question(self, 'Message',
                                     'Are you sure you want to quit?',
                                     QMessageBox.Yes | QMessageBox.No,
                                     QMessageBox.No)
        if reply == QMessageBox.Yes:
            self.close()
        else:
            e.ignore()
        
if __name__ == '__main__':
    app = QApplication(sys.argv)
    da = DA()
    sys.exit(app.exec_())