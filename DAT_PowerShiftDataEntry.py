import openpyxl
import pyodbc

### Creates a Database Object Cursor ###
def get_db_cursor(database='Database=LensData;'):
    driver = 'Driver={SQL Server Native Client 11.0};'
    server = 'Server=localhost;'
    trust = 'Trusted_Connection=yes;'

    try:
        db_con = pyodbc.connect(driver+server+database+trust)
    except:
        print('connection to database error')
        
    return db_con.cursor()


db_cursor = get_db_cursor('Database=LensData;')

wb = openpyxl.load_workbook('../DAT_Data/PowerShifts/BPS3.xlsx')
sheet = wb['Sheet1']
skipped = 0
entered = 0

for i in range(2,sheet.max_row+1,1):
    row = []
    for r in sheet[i]:
        if r.value == 'NULL' or r.value == 'NA' or r.value == 'N/A' or r.value == '' or r.value == None:
            row.append(None)
        else:
            row.append(r.value)
           
    #print(row)
    try:
        db_cursor.execute('''
                INSERT INTO PowerShift
                (BatchCode, ModelName, Diopter, ItemCode, OldModel, OldDiopter, OldItemCode, ButtonNo, FirstCutBFL, FirstCutLathe,
                SecondCutBFL, SecondCutLathe, ThirdCutRadius, ThirdCutLathe, IolaID)
                VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)''', row)
        print(f'Entered Batch: {row[0]}')
        entered += 1
        db_cursor.commit()
    except:
        print(f'Duplicate Batch Number: {row[0]}')
        skipped += 1
        
db_cursor.commit()
print(skipped)
print(entered)